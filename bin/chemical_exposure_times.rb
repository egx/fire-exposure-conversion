#!/usr/bin/env ruby

module ChemicalExposureTimes

  require 'optimist'
  require 'ap'
  require 'csv'

  opts = Optimist::options do
    opt :chemicals, "Chemical mapping file", type: String, required: true
    opt :exposure, "Exposures file", type: String, required: true
  end

  ap opts

  Optimist::die :chemicals, "must point to an existing file" unless File.exist?(opts[:chemicals])
  Optimist::die :exposure, "must point to an existing file" unless File.exist?(opts[:exposure])

  class Error
    @@errors = []
    attr_reader :msg, :row, :type

    def initialize(msg, row, type)
      @msg = msg
      @row = row
      @type = type

      @@errors << self
    end

    def display
      "Row #{row}: #{msg} (#{type})"
    end

    def self.errors
      @@errors
    end
  end

  # read chemicals file
  #   create hashmap: fire_type => [chemicals]

  fire_type_to_chemical = Hash.new { |h,k| h[k] = [] }
  File.open(opts[:chemicals]).each_line do |line|
    # puts line
    line.match(/^(.+?)\((.+)\)$/) do |m|
      m[2].split(';').each do |chemical|
        m[1].split('/').map(&:strip).each do |fire_type|
          fire_type_to_chemical[fire_type] << chemical.strip
        end
      end
    end
  end

  ap fire_type_to_chemical

  # read exposures file
  # for each entry
  #   get exposure time
  #   TODO: get PPE failure (not currently provided in exposuers file)
  #   get fire type
  #   for each fire type
  #     lookup chemicals
  #     for each chemical
  #       add exposure time to chemical in hashmap: chemical => total_exposure_time

  row_count = 0
  exposure_keys = []
  exposures = []
  CSV.foreach(opts[:exposure]) do |row|
    row_count += 1
    if row_count == 1
      exposure_keys = row.map(&:to_sym)
      next
    end

    joined_row = case
    when row.length == exposure_keys.length
      exposure_keys.zip(row)
    when row.length > exposure_keys.length
      Error.new("too many fields", row_count, :exposure_parsing)
      leftovers = row.pop(row.length - exposure_keys.length + 1)
      row.last << leftovers.join
      exposure_keys.zip(row)
    when (row.length + 1) == exposure_keys.length
      Error.new("missing one field", row_count, :exposure_parsing)
      exposure_keys.zip(row)
    else
      Error.new("missing multiple fields", row_count, :exposure_parsing)
      STDERR.puts "Row #{row_count} is missing more than one field: #{row}"
      nil
    end
    exposures << joined_row.to_h.merge(exposure_file_row: row_count) unless joined_row.nil?
  end

  ap exposures

  # collect cumulative exposure times by chemical
  cumulative_exposure_by_chemical = Hash.new { |h,k| h[k] = 0 }
  exposures.each do |exp|
    exposure_duration = exp[:se_duration_of_exposure].to_i
    if exposure_duration < 1
      Error.new("missing exposure duration", exp[:exposure_file_row], :exposure_calculation)
      STDERR.puts "Missing exposure duration at row #{exp[:exposure_file_row]}"
      next
    end

    exp[:se_type_of_fire].split(';').map(&:strip).each do |type|
      begin
        puts [type, exp[:exposure_file_row]].join(' ')
        fire_type_to_chemical.fetch(type).each do |chm|
          puts "    #{chm}"
          cumulative_exposure_by_chemical[chm] += exposure_duration
        end
      rescue IndexError
        Error.new("could not find chemicals for fire type: #{type}", exp[:exposure_file_row], :exposure_calculation)
        STDERR.puts "Could not find chemicals for fire type '#{type}' from row #{exp[:exposure_file_row]}"
      end
    end
  end

  ap cumulative_exposure_by_chemical

  Error.errors.each do |err|
    STDERR.puts err.display
  end

  # write output file
  output = [['Chemical', 'Minutes of Exposure']]
  output += cumulative_exposure_by_chemical.to_a

  exposure_tsv_file = output.map { |a| a.join("\t") }.join("\n")

  File.open('cumulative_exposures.tsv', 'w').write(exposure_tsv_file)
end
