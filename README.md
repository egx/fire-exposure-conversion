# Fire Incident Summary

A set of command line pipelines to convert *.XLSX dumps from the Fire Department's incident tracking and ticketing system to documents suitable for presentation in court.

## Preparing the Data

### Convert Excel Output to CSV

Install xlsx2csv

```zsh
cargo install xlsx2csv
```

Convert xlsx file to CSV

```zsh
xlsx2csv <xlsx>
```

**NOTE**: This can also be done by exporting to CSV from Excel, LibreOffice, etc.

### Correct CSV File

Two things need to be corrected in the generated CSV file:

1. There is an off by one error in the exported data.
2. Header names are unnecessarily long and difficult to work with.

**Header Names as Exported**

	1   Incident (Incident Details): Date of Service
	2   Ticket (Fields): Ticket Unit
	3   Smoke Exposure (Crew Member #1): Name
	4   Smoke Exposure (Crew Member #1): Employee ID
	5   Smoke Exposure (Crew Member #1): Rank
	6   Smoke Exposure (Crew Member #1): Type of Fire
	7   Smoke Exposure (Crew Member #1): Activity Performed
	8   Smoke Exposure (Crew Member #1): Duration of Exposure
	9   Smoke Exposure (Crew Member #1): Smoke Characteristics
	10  Smoke Exposure (Crew Member #1): Routes of Exposure
	11  Smoke Exposure (Crew Member #1): PPE Malfunction
	12  Fire Unit Narrative

The `Ticket Number` (col 1) header is missing from the above list and the data for `PPE Malfunction` (col 11) is not present. This means that all of the header values are shifted to one less than they should be except `Fire Unit Narrative`.

**Corrected Header Names**

These headers have also been shortened and had the spaces replaced with underscores to make them easier to work with.

	1   ticket_number
	2   incident_date_of_service
	3   ticket_unit
	4   se_name
	5   se_employee_id
	6   se_rank
	7   se_type_of_fire
	8   se_activity_performed
	9   se_duration_of_exposure
	10  se_smoke_characteristics
	11  se_routes_of_exposure
	12  fire_unit_narrative

We can fix the headers in the file with the following:

```zsh
cat field_list.txt > Smoke_Exposures_clean.csv
tail -n +2 Smoke_Exposures.csv >> Smoke_Exposures_clean.csv
```

**TODO**: We'll need to change how we handle data dumps with multiple crew members listed.

## Processing the Data

### Convert CSV to JSON

	csvjson Smoke_Exposures_clean.csv > Smoke_Exposures_clean.json

### Create Markdown from JSON

	jq -r ".[] | \"$(cat markdown_template.txt)\"" Smoke_Exposures_clean.json > Smoke_Exposures_clean.md

### Convert Markdown to DOCX

The pagebreak filter from [pandoc/lua-filters](https://github.com/pandoc/lua-filters) is necessary to render page breaks. The lua filters are available as a submodule of this repository.

Run:

	git submodule init

This will download the lua-filters submodule so that the following command can run:

	pandoc -f markdown --atx-headers -t docx -o Smoke_Exposures.docx --lua-filter=lua-filters/pagebreak/pagebreak.lua

### Alternate CSV to DOCX Pipeline

Alternately, we can use a pipeline to convert directly from the cleaned up CSV to a DOCX file.

	csvjson Smoke_Exposures_clean.csv \
	| jq -r ".[] | \"$(cat markdown_template.txt)\"" \
	| pandoc -f markdown --atx-headers -t docx -o Smoke_Exposures.docx --lua-filter=lua-filters/pagebreak/pagebreak.lua

## Collecting Cumulative Exposure Data

This section needs to be added. A custom Ruby script has been added as doing this via CLI programs is too cumbersome.
